# touchpadctl

![](https://img.shields.io/badge/written%20in-bash-blue)

Shell script to toggle laptop touchpad using xinput.

Can be bound to a hotkey using your window manager of choice.


## Download

- [⬇️ touchpadctl.sh.gz](dist-archive/touchpadctl.sh.gz) *(416B)*
